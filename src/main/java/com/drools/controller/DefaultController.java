package com.drools.controller;

import com.drools.entity.*;
import com.drools.repository.LocationRepository;
import com.drools.repository.ProductRepository;
import com.drools.repository.ShippingOptionRepository;
import com.drools.services.ShippingRateService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@RestController
public class DefaultController {

    @Autowired
    ShippingOptionRepository shippingOptionRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    ShippingRateService shippingRateService;

    @GetMapping("/")
    public ResponseEntity<?> defaultController(
            @RequestParam Integer shippingOptionId,
            @RequestParam(value = "productId") List<Integer> productIds,
            @RequestParam Integer userLocationId
    ) throws NotFoundException {

        // Preparing data to build order object
        var shippingOption = shippingOptionRepository.findById(shippingOptionId).orElseThrow(() -> new NotFoundException("Shipping option not found"));
        Set<Product> products = shippingOption.getProducts().stream().filter(product -> productIds.contains(product.getId())).collect(Collectors.toSet());
        BigDecimal orderTotal = products.stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        // End preparing data


        var order = new Order();
        order.setProducts(products);
        order.setTotal(orderTotal);
        order.setShippingOption(shippingOption);

        AtomicReference<List<ShippingRate>> shippingRates = new AtomicReference<>(new ArrayList<>());
        AtomicReference<Location> location = new AtomicReference<>();

        order.getShippingOption().getShippingZones().forEach(shippingZone -> shippingZone.getLocations().forEach(loc -> {
            if(loc.getId().equals(userLocationId)){
                location.set(loc);
                order.setLocation(loc);
                shippingRates.set(shippingZone.getRates());
            }
        }));

        shippingRateService.getShippingRateForOrder(order, shippingRates);

        HashMap<String, Object> response = new HashMap<>();
        response.put("location", location.get());
        response.put("rates", order.getShippingRates());

        return ResponseEntity.ok().body(response);
    }
}
