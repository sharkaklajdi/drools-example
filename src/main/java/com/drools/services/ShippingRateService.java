package com.drools.services;

import com.drools.entity.Order;
import com.drools.entity.ShippingRate;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ShippingRateService {
    private final KieContainer kieContainer;

    public ShippingRateService(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    public void getShippingRateForOrder(Order order, AtomicReference<List<ShippingRate>> shippingRates) {
        KieSession kieSession = kieContainer.newKieSession();
        kieSession.insert(order);
        kieSession.insert(shippingRates.get());
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
