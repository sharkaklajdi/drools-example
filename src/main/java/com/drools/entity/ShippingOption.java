package com.drools.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
public class ShippingOption {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    String name;

    @OneToMany(cascade = CascadeType.ALL)
    Set<Product> products;


    @OneToMany(mappedBy = "shippingOption", cascade = CascadeType.ALL, orphanRemoval = true)
    List<ShippingZone> shippingZones;
}
