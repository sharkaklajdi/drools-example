package com.drools.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity(name = "shipping_zone")
@Getter
@Setter
public class ShippingZone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "shippingZones")
    List<Warehouse> warehouses;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "shippingZones")
    List<Location> locations;

    @OneToMany(mappedBy = "shippingZone", cascade = CascadeType.ALL, orphanRemoval = true)
    List<ShippingRate> rates;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "shipping_option_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    ShippingOption shippingOption;
}
