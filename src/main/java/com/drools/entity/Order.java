package com.drools.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class Order {
    Set<Product> products;
    ShippingOption shippingOption;
    BigDecimal total;
    ShippingRate shippingRate;
    Location location;
    Set<ShippingRate> shippingRates = new HashSet<>();
}
