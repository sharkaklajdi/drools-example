package com.drools.repository;

import com.drools.entity.ShippingZone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingZoneRepository extends CrudRepository<ShippingZone, Integer> {
}
