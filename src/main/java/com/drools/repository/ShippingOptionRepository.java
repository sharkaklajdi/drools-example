package com.drools.repository;

import com.drools.entity.ShippingOption;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingOptionRepository extends CrudRepository<ShippingOption, Integer> {
}
