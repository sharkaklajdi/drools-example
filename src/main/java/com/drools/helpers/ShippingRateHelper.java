package com.drools.helpers;

import com.drools.entity.ShippingRate;

import java.util.List;
import java.util.Optional;

public class ShippingRateHelper {

    /**
     * Get shipping rate based on type
     *
     * @param rawShippingRates
     * @param type
     * @return
     */
    public static Optional<ShippingRate> getShippingRatesBasedOnType(List<ShippingRate> rawShippingRates, String type) {
        return rawShippingRates.stream().filter(shippingRate -> shippingRate.getType().equals(type)).findFirst();
    }
}
